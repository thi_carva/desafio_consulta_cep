# desafio_consulta_cep

Consultar Dados de um Endereço a partir de um CEP

# **Teste** 

1.  Criar uma funcionalidade para consultar os dados de um endereço a partir de um CEP. 

2.  Criar um cenário de sucesso na consulta, exibindo apenas o código do IBGE do endereço no stdout. 

3.  Criar um cenário passando um CEP inválido 

# **Endpoint para teste**

Utilizar a API: 


> https://viacep.com.br/ws/01001000/json/  


**Requisitos:** 

Utilizar a linguagem Ruby para codificação;

Realizar as requests com o HTTParty;

Escrever os cenários em gherkin;

Utilizar as boas práticas da automação;

**Observação**

Não existe automação certa ou errada, existem várias formas de se realizar uma mesma tarefa.

O objetivo deste teste é verificar somente o nível de conhecimento sobre as ferramentas e estruturação do projeto.

Após realizar os testes, subir para um repositório git de sua preferência e encaminhar o link para o e-mail:

[marina.jferreira@hsl.org.br](url)

